var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0],
  wall: [255, 255, 255]
}

var boardSize
var size = 23
var rule = -1
var arrayMaze = create2DArray(size, size, 0, true)
for (var i = 0; i < arrayMaze.length; i++) {
  for (var j = 0; j < arrayMaze[i].length; j++) {
    if (i === 0 || j === 0 || i === arrayMaze.length - 1 || j === arrayMaze[i].length - 1) {
      arrayMaze[i][j] = 1
    }
  }
}
var chamberStack = []
chamberStack.push([0, 0, size - 2, size - 2])
var orientation
var wallPos
var doorPos
var chamberX
var chamberY
var chamberW
var chamberH
var subChambers

var frames = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < arrayMaze.length; i++) {
    for (var j = 0; j < arrayMaze[i].length; j++) {
      noStroke()
      if (arrayMaze[i][j] === 1) {
        fill(colors.wall)
      } else if (arrayMaze[i][j] === 0) {
        fill(colors.dark)
      }
      rect(windowWidth * 0.5 + (i - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), windowHeight * 0.5 + (j - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), (44 / 768) * boardSize * (17 / size), (44 / 768) * boardSize * (17 / size))
    }
  }

  frames += deltaTime * 0.025
  if (frames > 1) {
    frames = 0
    if (chamberStack.length > 0) {
      if (chamberChecker(chamberStack[chamberStack.length - 1]) === true) {
        orientation = wallOrientation(chamberStack[chamberStack.length - 1])
        wallPos = wallPosition(chamberStack[chamberStack.length - 1], orientation)
        doorPos = doorPosition(chamberStack[chamberStack.length - 1], orientation)
        chamberX = chamberStack[chamberStack.length - 1][0]
        chamberY = chamberStack[chamberStack.length - 1][1]
        chamberW = chamberStack[chamberStack.length - 1][2]
        chamberH = chamberStack[chamberStack.length - 1][3]
        for (var i = chamberX + 1; i < chamberX + 1 + chamberW; i++) {
          for (var j = chamberY + 1; j < chamberY + 1 + chamberH; j++) {
            if (orientation === 0) {
              arrayMaze[chamberX + wallPos + 1][j] = 1
              arrayMaze[chamberX + wallPos + 1][chamberY + doorPos + 1] = 0
            } else {
              arrayMaze[i][chamberY + wallPos + 1] = 1
              arrayMaze[chamberX + doorPos + 1][chamberY + wallPos + 1] = 0
            }
          }
        }
        subChambers = getSubChambers(chamberStack[chamberStack.length - 1], orientation, wallPos, chamberX, chamberY, chamberW, chamberH)
        chamberStack.push(subChambers[0])
        chamberStack.push(subChambers[1])
        chamberStack.splice(-3, 1)
      } else {
        chamberStack.splice(-1, 1)
      }
    } else {
      size = 23 + Math.floor((mouseX / windowWidth) * 14) * 2
      if (rule < 3) {
        rule++
      } else {
        rule = -1
      }
      arrayMaze = create2DArray(size, size, 0, true)
      for (var i = 0; i < arrayMaze.length; i++) {
        for (var j = 0; j < arrayMaze[i].length; j++) {
          if (i === 0 || j === 0 || i === arrayMaze.length - 1 || j === arrayMaze[i].length - 1) {
            arrayMaze[i][j] = 1
          }
        }
      }
      chamberStack = []
      chamberStack.push([0, 0, size - 2, size - 2])
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function chamberChecker(stack) {
  var dividable
  var width = stack[2]
  var height = stack[3]
  if (width < 3 + rule || height < 3 + rule) {
    dividable = false
  } else {
    dividable = true
  }
  return dividable
}

function wallOrientation(stack) {
  var orientation
  var width = stack[2]
  var height = stack[3]
  if (width > height) {
    orientation = 0
  } else if (width < height) {
    orientation = 1
  } else if (width === height) {
    orientation = Math.floor(Math.random() * 2)
  }
  return orientation
}

function wallPosition(stack, orientation) {
  var position
  if (orientation === 0) {
    position = 1 + Math.floor(Math.random() * (stack[2] - rule) * 0.5) * 2
  } else {
    position = 1 + Math.floor(Math.random() * (stack[3] - rule) * 0.5) * 2
  }
  return position
}

function doorPosition(stack, orientation) {
  var position
  if (orientation === 0) {
    position = Math.floor(Math.random() * (stack[3] - rule) * 0.5) * 2
  } else {
    position = Math.floor(Math.random() * (stack[2] - rule) * 0.5) * 2
  }
  return position
}

function getSubChambers(stack, orientation, wallPos, chamberX, chamberY, chamberW, chamberH) {
  var subChamberA
  var subChamberB
  if (orientation === 0) {
    subChamberA = [chamberX, chamberY, wallPos, chamberH]
    subChamberB = [chamberX + wallPos + 1, chamberY, chamberW - wallPos - 1, chamberH]
  } else {
    subChamberA = [chamberX, chamberY, chamberW, wallPos]
    subChamberB = [chamberX, chamberY + wallPos + 1, chamberW, chamberH - wallPos - 1]
  }
  return [subChamberA, subChamberB]
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
